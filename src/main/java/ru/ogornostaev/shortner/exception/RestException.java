package ru.ogornostaev.shortner.exception;

/**
 * Created by ogornostaev 26.02.2017
 */
public abstract class RestException extends RuntimeException {

    RestException(String message) {
        super(message);
    }

    RestException(String message, Throwable cause) {
        super(message, cause);
    }
}
