package ru.ogornostaev.shortner.exception;

/**
 * Created by ogornostaev 26.02.2017
 */
public class InternalServiceException extends RestException {
    public InternalServiceException(String message) {
        super(message);
    }

    public InternalServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
