package ru.ogornostaev.shortner.service.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.model.dto.Account;
import ru.ogornostaev.shortner.model.rest.request.AccountRequest;
import ru.ogornostaev.shortner.model.rest.response.AccountResponse;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.service.AccountsService;
import ru.ogornostaev.shortner.service.PasswordGenerator;
import ru.ogornostaev.shortner.utils.PasswordHashGenerator;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class AccountAction implements ServiceAction<AccountRequest, AccountResponse> {

    private final Logger log = LoggerFactory.getLogger(AccountAction.class);

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private PasswordGenerator passwordGenerator;

    @Override
    public AccountResponse process(AccountRequest request) {
        String password = passwordGenerator.password();
        String passwordHash = PasswordHashGenerator.getPasswordHash(password);

        boolean success = accountsService.saveIfAbsent(new Account()
                .setAccountId(request.getAccountId())
                .setPasswordHash(passwordHash)
        );

        if (!success) {
            log.info("such account already exists: {}", request);
        }

        return new AccountResponse()
                .setSuccess(success)
                .setDescription(success ? "Your account is opened" :
                        "Account with that ID already exists")
                .setPassword(success ? password :
                        null);
    }

    @Override
    public boolean supports(Class<? extends RestRequest> requestClass) {
        return requestClass.equals(AccountRequest.class);
    }
}
