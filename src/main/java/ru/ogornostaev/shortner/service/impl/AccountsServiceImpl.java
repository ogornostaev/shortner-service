package ru.ogornostaev.shortner.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ogornostaev.shortner.model.dto.Account;
import ru.ogornostaev.shortner.repository.AccountsRepository;
import ru.ogornostaev.shortner.service.AccountsService;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class AccountsServiceImpl implements AccountsService {

    @Autowired
    private AccountsRepository accountsRepository;

    @Transactional
    public boolean saveIfAbsent(Account account) {
        boolean saved;

        if (saved = !accountsRepository.exists(account.getAccountId())) {
            accountsRepository.save(account);
        }

        return saved;
    }
}
