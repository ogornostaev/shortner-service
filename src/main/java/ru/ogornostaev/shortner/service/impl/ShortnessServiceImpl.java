package ru.ogornostaev.shortner.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ogornostaev.shortner.model.dto.Shortness;
import ru.ogornostaev.shortner.repository.ShortnessRepository;
import ru.ogornostaev.shortner.service.ShortnessService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class ShortnessServiceImpl implements ShortnessService {

    @Autowired
    private ShortnessRepository shortnessRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public boolean saveIsAbsent(Shortness shortness) {
        boolean saved;

        if (saved = !shortnessRepository.exists(shortness.getShortnessCode())) {
            shortnessRepository.save(shortness);
        }

        return saved;
    }

    @Transactional(readOnly = true)
    public Map<String, Integer> getRedirectsStatistics(String accountId) {

        final Map<String, Integer> statistics = new HashMap<>();
        jdbcTemplate.query(
                "SELECT * FROM shortness WHERE account_id = ?",
                rs -> {
                    statistics.put(
                            rs.getString("original_url"),
                            rs.getInt("redirects_count")
                    );
                },
                accountId
        );

        return statistics;
    }

    @Transactional
    public Shortness incrementAndGet(String shortnessCode) {

        int updated = jdbcTemplate.update(
                "UPDATE  shortness SET redirects_count=redirects_count+1 WHERE shortness_code=?",
                shortnessCode
        );

        Shortness shortness = null;
        if (updated > 0) {
            shortness = shortnessRepository.findOne(shortnessCode);
        }

        return shortness;
    }

}
