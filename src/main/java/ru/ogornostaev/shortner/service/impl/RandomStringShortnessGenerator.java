package ru.ogornostaev.shortner.service.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.service.ShortnessGenerator;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class RandomStringShortnessGenerator implements ShortnessGenerator {

    private final static int DEFAULT_SHORTNESS_LENGTH = 8;

    private int shortnessLength = DEFAULT_SHORTNESS_LENGTH;

    public RandomStringShortnessGenerator setShortnessLength(int shortnessLength) {
        this.shortnessLength = shortnessLength;
        return this;
    }

    @Override
    public String shortness() {
        return RandomStringUtils.random(shortnessLength, 0, 0, true, false);
    }
}
