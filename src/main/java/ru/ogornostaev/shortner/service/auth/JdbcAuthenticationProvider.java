package ru.ogornostaev.shortner.service.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.ogornostaev.shortner.repository.AccountsRepository;
import ru.ogornostaev.shortner.model.dto.Account;
import ru.ogornostaev.shortner.utils.PasswordHashGenerator;

import java.util.Collections;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class JdbcAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AccountsRepository accountsRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String accountId = authentication.getName();
        Object passwordCredentials = authentication.getCredentials();

        Account account = accountsRepository.findOne(accountId);

        if (account == null) {
            throw new UsernameNotFoundException("no such accountId: "  + accountId);
        }

        if (passwordCredentials == null || !StringUtils.hasText(passwordCredentials.toString())) {
            throw new BadCredentialsException("no or empty password");
        }

        String passwordHash = PasswordHashGenerator.getPasswordHash(passwordCredentials.toString());
        if (!passwordHash.equals(account.getPasswordHash())) {
            throw new BadCredentialsException("invalid password for accountId: " + accountId);
        }

        return new UsernamePasswordAuthenticationToken(accountId, passwordHash,
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
        );
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
