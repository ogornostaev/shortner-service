package ru.ogornostaev.shortner.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by ogornostaev 26.02.2017
 */
@Entity
@Table(name = "accounts")
public class Account implements Serializable {

    @Id
    @Column(name = "account_id")
    private String accountId;
    @Column(name = "password_hash")
    private String passwordHash;

    public String getAccountId() {
        return accountId;
    }

    public Account setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public Account setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }
}
