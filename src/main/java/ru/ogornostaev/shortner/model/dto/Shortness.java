package ru.ogornostaev.shortner.model.dto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ogornostaev 26.02.2017
 */
@Entity
@Table(name = "shortness")
public class Shortness implements Serializable {

    @Id
    @Column(name = "shortness_code")
    private String shortnessCode;

    @Column(name = "account_id")
    private String accountId;

    @Column(name = "original_url")
    private String originalUrl;

    @Column(name = "redirect_type")
    private int redirectType;

    @Column(name = "redirects_count")
    private int redirectsCount;

    public String getShortnessCode() {
        return shortnessCode;
    }

    public Shortness setShortnessCode(String shortnessCode) {
        this.shortnessCode = shortnessCode;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Shortness setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public Shortness setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
        return this;
    }

    public int getRedirectsCount() {
        return redirectsCount;
    }

    public Shortness setRedirectsCount(int redirectsCount) {
        this.redirectsCount = redirectsCount;
        return this;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public Shortness setRedirectType(int redirectType) {
        this.redirectType = redirectType;
        return this;
    }

    @Override
    public String toString() {
        return "Shortness{" +
                "shortnessCode='" + shortnessCode + '\'' +
                ", accountId='" + accountId + '\'' +
                ", originalUrl='" + originalUrl + '\'' +
                ", redirectType=" + redirectType +
                ", redirectsCount=" + redirectsCount +
                '}';
    }
}
