package ru.ogornostaev.shortner.model.rest.response;

import ru.ogornostaev.shortner.model.rest.RestResponse;

/**
 * Created by ogornostaev 26.02.2017
 */
public class AccountResponse implements RestResponse {
    private boolean success;
    private String description;
    private String password;

    public boolean isSuccess() {
        return success;
    }

    public AccountResponse setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AccountResponse setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AccountResponse setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "AccountResponse{" +
                "success=" + success +
                ", description='" + description + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
