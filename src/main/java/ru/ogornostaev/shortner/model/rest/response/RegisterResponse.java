package ru.ogornostaev.shortner.model.rest.response;

import ru.ogornostaev.shortner.model.rest.RestResponse;

/**
 * Created by ogornostaev 26.02.2017
 */
public class RegisterResponse implements RestResponse {
    private String shortUrl;

    public String getShortUrl() {
        return shortUrl;
    }

    public RegisterResponse setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
        return this;
    }

    @Override
    public String toString() {
        return "RegisterResponse{" +
                "shortUrl='" + shortUrl + '\'' +
                '}';
    }
}
