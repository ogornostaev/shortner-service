package ru.ogornostaev.shortner.model.rest.request;

import ru.ogornostaev.shortner.model.rest.RestRequest;

/**
 * Created by ogornostaev 26.02.2017
 */
public class StatisticsRequest implements RestRequest {

    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public StatisticsRequest setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    @Override
    public String toString() {
        return "StatisticsRequest{" +
                "accountId='" + accountId + '\'' +
                '}';
    }

}
