package ru.ogornostaev.shortner.model.rest.request;

import ru.ogornostaev.shortner.model.rest.RestRequest;

/**
 * Created by ogornostaev 26.02.2017
 */
public class RedirectRequest implements RestRequest {
    private String shortnessCode;

    public String getShortnessCode() {
        return shortnessCode;
    }

    public RedirectRequest setShortnessCode(String shortnessCode) {
        this.shortnessCode = shortnessCode;
        return this;
    }

    @Override
    public String toString() {
        return "RedirectRequest{" +
                "shortnessCode='" + shortnessCode + '\'' +
                '}';
    }
}
