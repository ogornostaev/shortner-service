package ru.ogornostaev.shortner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ShortnerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShortnerServiceApplication.class, args);
	}
}
