## Installation ##
1. cd <project parent directory>
2. mvn spring-boot:run

## About ##
The service built on Spring Boot.
It starts with embeded Tomcat on ***8080*** HTTP port. 
Uses embeded H2 database located in ***~/shortner*** file.

## Using ##
### Registering account ###

```
#!

POST /account HTTP/1.1
Content-Type: application/json

{'AccountId': 'test'}

```

Response:

```
#!javascript

{'success': 'true', 'description': 'Your account is opened', 'password': 'xC345Fc0'}
```

### Registering shortness ###

For authorization specify Authorization header with Base64 decoded account password pair 

```
#!

POST /register HTTP/1.1
Content-Type: application/json
Authorization: Basic dGVzdDp4QzM0NUZjMA==

{ 'url': 'http://stackoverflow.com/questions/1567929/website-safe-data-access-architecture-question?rq=1', 'redirectType': 301}

```

Response:

retuns short URL, for example:

```
#!javascript

{'shortUrl': 'http://localhost:8080/xYswlE'}
```

### Using shortness ###
Executing short URL redirects to original url with specified redirect type.
If shortness is not registered will be returned 404 NOT FOUND HTTP status

### Viewing statistics ###

For viewing redirects statistic execute GET HTTP request with specified AccountId in path and Basic authorization.

```
#!

GET /statistic/test HTTP/1.1
Content-Type: application/json
Authorization: Basic dGVzdDp4QzM0NUZjMA==

```

Response:

retuns statistics map by original URL, for example:

```
#!javascript


{
'http://myweb.com/someverylongurl/thensomedirectory/': 10,
'http://myweb.com/someverylongurl2/thensomedirectory2/': 4,
'http://myweb.com/someverylongurl3/thensomedirectory3/': 91,
}

```